package com.arno.hive.udf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.MapObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by arno on 16/11/22.
 */
@Deprecated
public class WithUDTF extends GenericUDTF {
    static final Log logger = LogFactory.getLog(WithUDTF.class.getName());

    private TimeType time_type = null;
    private List<Integer> time_delta = null;
    private Map<Time, Set<Dims>> index = new HashMap<Time, Set<Dims>>();
    private Map<Time, Map<Dims, Values>> numericalMap = new HashMap<Time, Map<Dims, Values>>();
    private Map<Time, Map<Dims, Values>> percentMap = new HashMap<Time, Map<Dims, Values>>();

    private Map<Time, Map<Dims, Values>> numericalWithMap = new HashMap<Time, Map<Dims, Values>>();
    private Map<Time, Map<Dims, Values>> percentWithMap = new HashMap<Time, Map<Dims, Values>>();


    /*
     * 参数：
     * 0. 时间字段，例如partition_date
     * 1. 时间类型，表示时间字段的类型，取值范围为 "date", "week", "month"
     * 2. 时间间隔，字符串类型，用","分隔，例如 "1,7"
     * 3. 维度，需要一个map，格式为{"d1": "d1_value", "d2": 10, "d3": "d3_value"}
     * 4. 数值类型参数，需要一个map，格式为{"v1": 10, "v2": 20}
     * 5. 百分比类型参数，需要一个map，格式为{"v3": 0.1, "v4": 0.8}
     *
     *
     * 输出：
     * 1. time 时间字段
     * 2. values 维度，跟传入的map结构一样
     * 3. numerical_with 数值型参数的同环比
     * 4. percent_with 百分比类型的同环比
     */
    @Override
    public StructObjectInspector initialize(ObjectInspector[] argOIs) throws UDFArgumentException {
        logger.info("initialize");
//        if (argOIs.length != 6) {
//            throw new UDFArgumentLengthException("需要输入6个参数!");
//        }

        // TODO 额外的参数检查

        // 输出
        MapObjectInspector mapObjectInspector = ObjectInspectorFactory.getStandardMapObjectInspector(
                PrimitiveObjectInspectorFactory.javaStringObjectInspector,
                PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        ArrayList<String> fieldNames = new ArrayList<String>();
        ArrayList<ObjectInspector> fieldOIs = new ArrayList<ObjectInspector>();
        fieldNames.add("origin_time");
        fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldNames.add("dims");
        fieldOIs.add(mapObjectInspector);
        fieldNames.add("numerical_with");
        fieldOIs.add(mapObjectInspector);
        fieldNames.add("percent_with");
        fieldOIs.add(mapObjectInspector);
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);
    }

    /**
     * Give a set of arguments for the UDTF to process.
     *
     * @param args object array of arguments
     */
    @Override
    public void process(Object[] args) throws HiveException {
        int i = 0;
        for (Object arg: args) {
            logger.info(String.format("index: %d, arg: %s", i, arg));
            i++;
        }

        if (time_type == null) {
            String dateData = args[1].toString();
            logger.info("args[1]: " + dateData);
            if ("date".equals(dateData)) {
                time_type = TimeType.DATE;
            } else if ("week".equals(dateData)) {
                time_type = TimeType.WEEK;
            } else if ("month".equals(dateData)) {
                time_type = TimeType.MONTH;
            } else {
                throw new HiveException("参数1应该是时间类型，需要为 \"date\", \"week\", \"month\" 中的一个");
            }
        }
        if (time_delta == null) {
            time_delta = new ArrayList<Integer>();
            String time_delta_str = args[2].toString();
            for (String each: time_delta_str.split(",")) {
                time_delta.add(new Integer(each));
            }
        }

        String timeData = args[0].toString();
        Map<Text, Object> dimsData = ((Map<Text, Object>)args[3]);
        if (dimsData == null) {
            dimsData = new HashMap<Text, Object>();
        }
        Map<Text, Object> numericalData = (Map<Text, Object>)args[4];
        if (numericalData == null) {
            numericalData = new HashMap<Text, Object>();
        }
        Map<Text, Object> percentData = (Map<Text, Object>)args[5];
        if (percentData == null) {
            percentData = new HashMap<Text, Object>();
        }

        // 设置时间
        Time time = new Time(time_type, timeData);

        // 添加维度
        Dims dims = new Dims();
        for (Map.Entry<Text, Object> entry: dimsData.entrySet()) {
            dims.addDim(entry.getKey().toString(), entry.getValue().toString());
        }

        logger.info("dims: " + dims);

        // 添加索引
        if (!index.containsKey(time)) {
            HashSet<Dims> dims_set = new HashSet<Dims>();
            dims_set.add(dims);
            index.put(time, dims_set);
        } else {
            if (!index.get(time).add(dims)) {
                throw new HiveException(String.format("时间: [%s] 内存在相同维度组合: %s", time, dims));
            }
        }

        // 添加数值类型
        Values numericalValues = new Values();
        for (Map.Entry<Text, Object> entry: numericalData.entrySet()) {
            if (entry.getValue() == null) {
                logger.info("entry.getValue() is null");
                numericalValues.addValue(entry.getKey().toString(), null);
            } else if (entry.getValue() instanceof LongWritable) {
                numericalValues.addValue(entry.getKey().toString(), ((LongWritable)entry.getValue()).get());
            } else if (entry.getValue() instanceof DoubleWritable) {
                numericalValues.addValue(entry.getKey().toString(), ((DoubleWritable)entry.getValue()).get());
            } else if (entry.getValue() instanceof FloatWritable) {
                numericalValues.addValue(entry.getKey().toString(), ((FloatWritable)entry.getValue()).get());
            } else {
                logger.info("value type: " + entry.getValue().getClass());
                throw new HiveException("value must be LongWritable or DoubleWritable or FloatWritable");
            }
        }

        if (numericalMap.containsKey(time)) {
            numericalMap.get(time).put(dims, numericalValues);
        } else {
            Map<Dims, Values> item = new HashMap<Dims, Values>();
            item.put(dims, numericalValues);
            numericalMap.put(time, item);
        }
        logger.info("numericalMap: " + numericalMap);

        // 添加百分比类型
        Values percentValues = new Values();
        for (Map.Entry<Text, Object> entry: percentData.entrySet()) {
            if (entry.getValue() == null) {
                logger.info("entry.getValue() is null");
                numericalValues.addValue(entry.getKey().toString(), null);
            } else if (entry.getValue() instanceof LongWritable) {
                percentValues.addValue(entry.getKey().toString(), ((LongWritable)entry.getValue()).get());
            } else if (entry.getValue() instanceof DoubleWritable) {
                percentValues.addValue(entry.getKey().toString(), ((DoubleWritable)entry.getValue()).get());
            } else if (entry.getValue() instanceof FloatWritable) {
                percentValues.addValue(entry.getKey().toString(), ((FloatWritable)entry.getValue()).get());
            } else {
                logger.info("value type: " + entry.getValue().getClass());
                throw new HiveException("value must be LongWritable or DoubleWritable or FloatWritable");
            }
        }
        if (percentMap.containsKey(time)) {
            percentMap.get(time).put(dims, percentValues);
        } else {
            Map<Dims, Values> item = new HashMap<Dims, Values>();
            item.put(dims, percentValues);
            percentMap.put(time, item);
        }
    }

    /**
     * Called to notify the UDTF that there are no more rows to process.
     * Clean up code or additional forward() calls can be made here.
     */
    @Override
    public void close() throws HiveException {
        logger.info("time_type: " + time_type);
        logger.info("time_delta: " + time_delta);
        logger.info("numericalMap: " + numericalMap);
        logger.info("percentMap: " + percentMap);

        for (Integer delta: time_delta) {
            for (Map.Entry<Time, Map<Dims, Values>> entry : numericalMap.entrySet()) {
                Time time = entry.getKey();
                if (!numericalWithMap.containsKey(time)) {
                    numericalWithMap.put(time, new HashMap<Dims, Values>());
                }
                for (Map.Entry<Dims, Values> entry1 : entry.getValue().entrySet()) {
                    Dims dims = entry1.getKey();
                    Values values = entry1.getValue();
                    if (!numericalWithMap.get(time).containsKey(dims)) {
                        numericalWithMap.get(time).put(dims, new Values());
                    }
                    for (Map.Entry<String, Number> entry2 : values.getValues().entrySet()) {
                        String name = entry2.getKey();
                        Number value = entry2.getValue();
                        numericalWithMap.get(time).get(dims).addValue(name, value);

                        String diff_name = String.format("%s_delta_%d", name, delta);
                        Double diff_value = null;
                        logger.info(String.format("try time: %s, delta: %d, dims: %s, name: %s", time, delta, dims, name));
                        try {
                            // 查找是否存在对应条目
                            Number delta_value = numericalMap.get(time.delta(delta)).get(dims).getValue(name);
                            logger.info("get delta_value: " + delta_value);

                            // 计算差值
                            diff_value = (value.doubleValue() - delta_value.doubleValue()) / delta_value.doubleValue();
                            logger.info("get diff_value: " + diff_value);

                            if (Double.isNaN(diff_value) || Double.isInfinite(diff_value)) {
                                diff_value = null;
                            }
                        } catch (Exception e) {
                            logger.info(e);
                        }
                        numericalWithMap.get(time).get(dims).addValue(diff_name, diff_value);
                    }
                }
            }
        }
        logger.info("numericalWithMap" + numericalWithMap);

        for (Integer delta: time_delta) {
            for (Map.Entry<Time, Map<Dims, Values>> entry : percentMap.entrySet()) {
                Time time = entry.getKey();
                if (!percentWithMap.containsKey(time)) {
                    percentWithMap.put(time, new HashMap<Dims, Values>());
                }
                for (Map.Entry<Dims, Values> entry1 : entry.getValue().entrySet()) {
                    Dims dims = entry1.getKey();
                    Values values = entry1.getValue();
                    if (!percentWithMap.get(time).containsKey(dims)) {
                        percentWithMap.get(time).put(dims, new Values());
                    }
                    for (Map.Entry<String, Number> entry2 : values.getValues().entrySet()) {
                        String name = entry2.getKey();
                        Number value = entry2.getValue();
                        percentWithMap.get(time).get(dims).addValue(name, value);

                        String diff_name = String.format("%s_delta_%d", name, delta);
                        Double diff_value = null;
                        logger.info(String.format("try time: %s, delta: %d, dims: %s, name: %s", time, delta, dims, name));
                        try {
                            // 查找是否存在对应条目
                            Number delta_value = percentMap.get(time.delta(delta)).get(dims).getValue(name);
                            logger.info("get delta_value: " + delta_value);

                            // 计算差值
                            diff_value = (value.doubleValue() - delta_value.doubleValue());
                            logger.info("get diff_value: " + diff_value);
                        } catch (Exception e) {
                            logger.info(e);
                        }
                        percentWithMap.get(time).get(dims).addValue(diff_name, diff_value);
                    }
                }
            }
        }
        logger.info("percentWithMap" + percentWithMap);

        for (Map.Entry<Time, Set<Dims>> entry: index.entrySet()) {
            Time time = entry.getKey();
            for (Dims dims: entry.getValue()) {
                Object forwardObj[] = new Object[4];
                forwardObj[0] = time.getTime();
                forwardObj[1] = dims.getDims();

                // 数值型的结果
                Map<String, String> numericalResultMap = new HashMap<String, String>();
                for (Map.Entry<String, Number> entry1: numericalWithMap.get(time).get(dims).getValues().entrySet()) {
                    String name = entry1.getKey();
                    Number value = entry1.getValue();
                    if (numericalMap.get(time).get(dims).getValues().containsKey(name)) {
                        // 是原始指标
                        numericalResultMap.put(name, String.valueOf(value));
                        numericalResultMap.put(name + "_formatted", String.valueOf(value));
                    } else {
                        // 计算出来的同环比
                        String value_str = "";
                        if (value == null) {
//                            value_str = "--";
                            value_str = "0";
                        } else if (value.floatValue() > 0) {
//                            value_str = String.format("<font color=\"green\">%.2f%%</font>", value.floatValue()*100);
                            value_str = String.format("%.2f", value.floatValue()); // 星空
                        } else {
//                            value_str = String.format("<font color=\"red\">%.2f%%</font>", value.floatValue()*100);
                            value_str = String.format("%.2f", value.floatValue()); // 星空
                        }
                        numericalResultMap.put(name, String.valueOf(value));
                        numericalResultMap.put(name + "_formatted", value_str);
                    }
                }
                forwardObj[2] = numericalResultMap;

                // 百分比型的结果
                Map<String, String> percentResultMap = new HashMap<String, String>();
                for (Map.Entry<String, Number> entry1: percentWithMap.get(time).get(dims).getValues().entrySet()) {
                    String name = entry1.getKey();
                    Number value = entry1.getValue();
                    if (percentMap.get(time).get(dims).getValues().containsKey(name)) {
                        // 是原始指标
                        percentResultMap.put(name, value.toString());
                        percentResultMap.put(name + "_formatted", String.format("%.2f%%", value.floatValue()*100));
                    } else {
                        // 计算出来的同环比
                        String value_str = "";
                        if (value == null) {
//                            value_str = "--";
                            value_str = "0";
                        } else if (value.floatValue() > 0) {
//                            value_str = String.format("<font color=\"green\">%d</font>", Math.round(value.floatValue()*10000));
                            value_str = String.format("%d", Math.round(value.floatValue()*10000));
                        } else {
//                            value_str = String.format("<font color=\"red\">%d</font>", Math.round(value.floatValue()*10000));
                            value_str = String.format("%d", Math.round(value.floatValue()*10000));
                        }
                        percentResultMap.put(name, String.valueOf(value));
                        percentResultMap.put(name+"_formatted", value_str);
                    }
                }
                forwardObj[3] = percentResultMap;
                forward(forwardObj);
            }
        }
    }
}

enum TimeType {DATE, WEEK, MONTH};

class Time {
    private TimeType timeType;
    private String time;

    public Time(TimeType timeType, String time) {
        this.timeType = timeType;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    /**
     * @param num 为正数，1表示之前一个周期
     * @return
     * @throws ParseException
     */
    public Time delta(Integer num) {
        if (timeType == TimeType.DATE) {
            Date now;
            try {
                now = new SimpleDateFormat("yyyy-MM-dd").parse(time);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1 * num);
            Date delta = cal.getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String delta_time = formatter.format(delta);
            return new Time(timeType, delta_time);
        } else if (timeType == TimeType.WEEK) {
            String delta_time = String.valueOf(Integer.parseInt(time) - num);
            return new Time(timeType, delta_time);
        } else if (timeType == TimeType.MONTH) {
            String delta_time = String.valueOf(Integer.parseInt(time) - num);
            return new Time(timeType, delta_time);
        } else {
            throw new RuntimeException("unknown timeType: " + timeType);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Time oTime = (Time)obj;
        return this.timeType.equals(oTime.timeType)
                && this.time.equals(oTime.time);
    }

    @Override
    public int hashCode() {
        return this.timeType.hashCode() + this.timeType.hashCode();
    }

    @Override
    public String toString() {
        return "timeType: " + timeType.toString() + ", time: " + time;
    }
}

class Dims {
    private Map<String, String> dims = new HashMap<String, String>();

    public Map<String, String> getDims() {
        return dims;
    }

    public void addDim(String name, String value) {
        dims.put(name, value);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Dims oDims = (Dims)obj;
        return this.dims.equals(oDims.dims);
    }

    @Override
    public int hashCode() {
        return this.dims.hashCode();
    }

    @Override
    public String toString() {
        return dims.toString();
    }
}

class Values {
    private Map<String, Number> values = new HashMap<String, Number>();

    public void addValue(String name, Number value) {
        values.put(name, value);
    }

    public Map<String, Number> getValues() {
        return values;
    }

    public Number getValue(String name) {
        return values.get(name);
    }

    @Override
    public String toString() {
        return values.toString();
    }
}
