package com.arno.hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * Created by arno on 2016/12/7.
 */
public class PercentUDF extends UDF {
    public String evaluate(Double now_num) {
        try {
            return String.format("%.2f%%", now_num*100);
        } catch (Exception e) {
            return "--";
        }
    }
}
