package com.arno.hive.udf;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentTypeException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorConverters;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.LongWritable;

/**Description: 生成GID函数
 * Date:2016-02-26
 * @author yanweiran
 * @version 1.1
 * */


@Description(name = "generateGid",
        value = "_FUNC_(col1,value,col2,value,col3,value.....) - if the value of col1 equal -65535 or null" +
                "return 0, else return 1,and finally, transfer value to decimal",
        extended = "Example:\n"
                + "  > SELECT _FUNC_(date,name,age,salary) FROM src LIMIT 1;\n")


public class GenerateGidUDF extends GenericUDF {
    static final Log logger = LogFactory.getLog(LongestContinuousUDAF.class.getName());
    private transient ObjectInspectorConverters.Converter[] converters;



    public GenerateGidUDF() {
        //无参构造函数
    }

    @Override
    public String getDisplayString(String[] children) {
        assert (0 == children.length % 2);
        return "Generate gid of col";
    }



    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {

        //断言,判断传入参数个数是否为偶数个
        assert(arguments.length % 2 == 0 );

        String arrayStr = ""; //用来存储数位上的值

        for (int i = 0; i < arguments.length; i=i+2) {
            Boolean exclusive = (arguments[i].get() == null) ^ (arguments[i + 1].get() == null);

            if (exclusive) {
                arrayStr = arrayStr + "1";
            } else {
                if (String.valueOf(arguments[i].get()).equals(String.valueOf(arguments[i + 1].get()))) {
                    arrayStr = arrayStr + "0";
                } else {
                    arrayStr = arrayStr + "1";
                }

            }
        }

        return this.transDecimal(arrayStr);
    }


    /**
     * 该函数重载父类参数,目的是为了初始化传进来的参数
     * @param arguments 传进来的参数,列的名字和值,必须成对出现,且不能超过63对,包含63
     * @return 返回生成的gid
     */
    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        /*
        * 判断传进来的参数是否为偶数,如果是奇数则抛出异常
        * */
        if (arguments.length > 63)
            throw new UDFArgumentLengthException("_FUNC_ expects exactly less than 64 arguments");
        /*
        * 如果传入的参数对数大于64,则抛出异常
        * */
        if (arguments.length % 2 != 0)
            throw new UDFArgumentLengthException("_FUNC_ expects  pair of parameters should not exceed 64 ");

        for (int i = 0; i < arguments.length; i=i+1) {
            if (arguments[i].getCategory() != ObjectInspector.Category.PRIMITIVE) {
                throw new UDFArgumentTypeException(i,
                        "A string argument was expected but an argument of type " + arguments[i].getTypeName()
                                + " was given.");

            }
            PrimitiveObjectInspector.PrimitiveCategory primitiveCategory = ((PrimitiveObjectInspector) arguments[i])
                    .getPrimitiveCategory();
        }

        converters = new ObjectInspectorConverters.Converter[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            converters[i] = ObjectInspectorConverters.getConverter(arguments[i],
                    PrimitiveObjectInspectorFactory.writableLongObjectInspector);
        }

        // We will be returning a Long object
        return PrimitiveObjectInspectorFactory.writableLongObjectInspector;
    }


    /**
    * 将位向量转化成10进制数
     * @param  arrayStr 位向量
    */
    public LongWritable transDecimal(String arrayStr) {
        LongWritable intWritable = new LongWritable(0);
        long intResult = 0;
        for (int i = 0; i < arrayStr.length(); i++) {
            intResult += Math.pow(2, i) * (arrayStr.charAt(i) - 48);
        }

        intWritable.set(intResult);
        return intWritable;
    }

}



