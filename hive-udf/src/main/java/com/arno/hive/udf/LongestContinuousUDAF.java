package com.arno.hive.udf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDAF;
import org.apache.hadoop.hive.ql.exec.UDAFEvaluator;
import org.apache.hadoop.hive.ql.metadata.HiveException;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by arno on 16/1/23.
 *
 */
@Description(name = "longest_continuous",
        value = "_FUNC_(compare, key, target) - 按照compare排序，然后统计某个key等于target时的最长连续次数" +
                "compare: string 用于排序，若compare为null，该条记录会被忽略" +
                "key: string 统计的key" +
                "target: string 需要检测的目标值，target必须是静态值，且不为null，例如\"1\", ",
        extended = "查找成功纪录中使用pay_class_id=3的最长连续次数" +
                "ADD JAR hdfs:///user/hadoop-pay/user_upload/hive-udf-1.0-SNAPSHOT.jar;\n" +
                "CREATE TEMPORARY FUNCTION longest_continuous as 'com.arno.hive.udf.LongestContinuousUDAF';" +
                "INSERT overwrite TABLE `$target.table`" +
                "select\n" +
                "    user_id,\n" +
                "    longest_continuous(order_time, pay_class_id, \"3\")\n" +
                "from ba_pay.mid_trade_tradeflow\n" +
                "where is_suc = 1 -- and user_id = 15827 \n" +
                "group by user_id\n\n")
public class LongestContinuousUDAF extends UDAF {
    static final Log logger = LogFactory.getLog(LongestContinuousUDAF.class.getName());

    public static class LongestContinuousUDAFEvaluator implements UDAFEvaluator{

        public static class Aggr {
            private String target;
            private ArrayList<String> itemList;

            public Aggr() {
                itemList = new ArrayList<String>();
            }

            public void addItem(Item item) {
//                logger.info("addItem item: " + item);
//                logger.info("begore addItem itemList: " + itemList);
                itemList.add(item.toString());
//                logger.info("after addItem itemList: " + itemList);
            }

            public void addAllItem(ArrayList<String> itemList) {
//                logger.info("addAllItem");
//                logger.info("itemList: " + itemList);
//                logger.info("begore addAllItem itemList: " + this.itemList);
                this.itemList.addAll(itemList);
//                logger.info("after addAllItem itemList: " + this.itemList);
            }

            public ArrayList<String> getItemList() {
                return itemList;
            }

            public String getTarget() {
                return target;
            }

            public void setTarget(String target) {
                this.target = target;
            }

            public int getLongest() {
                Collections.sort(itemList);

//                logger.info("getLongest");
//                logger.info("target: " + target);
//                logger.info("itemList: " + itemList);

                int now = 0;
                int longest = 0;
                String last = "";
                int count = 0;
                for (String s : itemList) {
                    Item item = new Item(s);
                    if (target.equals(item.getKey())) {
                        // 本记录是target
                        if (target.equals(last)) {
                            // 上一条也是target
//                            logger.info("action 1 1");
                            count += 1;
                        } else {
//                            logger.info("action 1 0");
                            // 上一条不是target
                            count = 1;
                            last = item.getKey();
                        }
                    } else {
                        // 本条记录不是target
                        if (target.equals(last)) {
//                            logger.info("action 0 1");
                            // 上一条是target
                            longest = Math.max(longest, count);
                            count = 0;
                            last = "";
                        } else {
//                            logger.info("action 0 1");
                            // 上一条不是target
                            count = 0;
                            last = "";
                        }
                    }
                    now += 1;

//                    logger.info("now: " + now);
//                    logger.info("item: " + item);
//                    logger.info("longest: " + longest);
//                    logger.info("last: " + last);
//                    logger.info("count: " + count);
                }
                longest = Math.max(longest, count);
//                logger.info("final longest: " + longest);
                return longest;
            }
        }

        /**
         * Use item class to serialize intermediate computation
         */
        public static class Item implements Comparable<Item> {
            private String compare;
            private String key;
            private final static String SPLIT_KEY = "\r\n\r\n";

            public Item() {

            }

            public Item(String s) {
                String[] splitResult = s.split(SPLIT_KEY);
                compare = splitResult[0];
                key = splitResult[1];
            }

            public String getCompare() {
                return compare;
            }

            public void setCompare(String compare) {
                this.compare = compare;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String toString() {
                return String.format("%s%s%s", compare, SPLIT_KEY, key);
            }

            @Override
            public int compareTo(Item o) {
                return this.compare.compareTo((o).compare);
            }
        }

        private Aggr aggr = null;
        /**
         * function: Constructor
         */
        public LongestContinuousUDAFEvaluator(){
            super();
            init();
        }

        /**
         * function: init()
         * Its called before records pertaining to a new group are streamed
         */
        public void init() {
            aggr = new Aggr();
        }

        /**
         * function: iterate
         * This function is called for every individual record of a group
         * @return
         * @throws HiveException
         */
        public boolean iterate(String compare, String key, String target) throws HiveException{
//            logger.info("compare: " + compare + ", key: " + key + ", target: " + target);
            if (compare == null) {
                // 忽略掉compare包含null的记录
                logger.warn(String.format("ignore record: compare=%s, key=%s, target=%s", compare, key, target));
                return true;
            }
            aggr.setTarget(target);
            Item item = new Item();
            item.setCompare(compare);
            item.setKey(key);
            aggr.addItem(item);
            return true;
        }

        /**
         * function: terminate
         * this function is called after the last record of the group has been streamed
         * @return
         */
        public int terminate(){
            return aggr.getLongest();
        }

        /**
         * function: terminatePartial
         * this function is called on the mapper side and
         * returns partially aggregated results.
         * @return
         */
        public Aggr terminatePartial(){
            return aggr;
        }

        /**
         * function: merge
         * This function is called two merge two partially aggregated results
         * @param another
         * @return
         */
        public boolean merge(Aggr another){
            if(another == null) return true;
//            logger.info("merge this.aggr: " + this.aggr.hashCode() + ", another.aggr: " + another.hashCode());
            this.aggr.addAllItem(another.getItemList());
            this.aggr.setTarget(another.getTarget());
            return true;
        }
    }
}
