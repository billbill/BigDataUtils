package com.arno.hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

/**
 * Created by arno on 16/11/24.
 */
public class ChrUDF extends UDF {
    private Text result = new Text();

    public Text evaluate(IntWritable ascii_number) {
        if (ascii_number == null) {
            return null;
        }

        result.set(Character.toString((char) ascii_number.get()));
        return result;
    }
}
