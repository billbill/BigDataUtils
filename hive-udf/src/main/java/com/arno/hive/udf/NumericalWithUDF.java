package com.arno.hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * Created by arno on 2016/12/7.
 */
public class NumericalWithUDF extends UDF {
    public String evaluate(Double now_num, Double diff_num) {
        try {
            Double rate = (now_num - diff_num) / diff_num;
            if (rate.isInfinite()){
                return "--";
            } else if (rate.isNaN()) {
                return "--";
            } else if (rate > 0) {
                return String.format("<font color=\"green\">%.2f%%</font>", rate*100);
            } else if (rate < 0){
                return String.format("<font color=\"red\">%.2f%%</font>", rate*100);
            } else {
                return "--";
            }
        } catch (Exception e) {
            return "--";
        }
    }
}
