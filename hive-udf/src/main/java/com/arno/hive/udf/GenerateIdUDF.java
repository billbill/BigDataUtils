package com.arno.hive.udf;

import com.google.common.base.Joiner;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;

import java.util.ArrayList;

/**
 * Created by arno on 16/3/2.
 */
public class GenerateIdUDF extends GenericUDF {

    /**
     * Initialize this GenericUDF. This will be called once and only once per
     * GenericUDF instance.
     *
     * @param arguments The ObjectInspector for the arguments
     * @return The ObjectInspector for the return value
     * @throws UDFArgumentException Thrown when arguments have wrong types, wrong length, etc.
     */
    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        assert(arguments.length % 2 == 0 );

        return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
    }

    /**
     * Evaluate the GenericUDF with the arguments.
     *
     * @param arguments The arguments as DeferedObject, use DeferedObject.get() to get the
     *                  actual argument Object. The Objects can be inspected by the
     *                  ObjectInspectors passed in the initialize call.
     * @return The
     */
    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        ArrayList<String> argvList = new ArrayList<String>();
        for (int i=0; i<arguments.length; i+=2) {
            argvList.add(Joiner.on("=").join(String.valueOf(arguments[i].get()), String.valueOf(arguments[i+1].get())));
        }
        return new Text(Joiner.on(";").join(argvList));
    }

    /**
     * Get the String to be displayed in explain.
     *
     * @param children
     */
    @Override
    public String getDisplayString(String[] children) {
        return "GenerateIdUDF";
    }
}
