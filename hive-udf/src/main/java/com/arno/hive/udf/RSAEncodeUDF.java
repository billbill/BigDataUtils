package com.arno.hive.udf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by arno on 16/3/2.
 */
public class RSAEncodeUDF extends GenericUDF {
    static final Log logger = LogFactory.getLog(RSAEncodeUDF.class.getName());

    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqYjh9jHWMRaiVMweMSbbcoulC/BsIzOa942r90XUn0ETuc6yKuflPvoj/APaTsDxORCKnuSzewi3zleildXyJz0h7Y/0i/rrtuq9e8feUuUr6Uw1xHJNfi1N0VareDW7lTbI1b06wBQyljwuYtACwRI4Xw9wSOapEUNoTrz4ayQIDAQAB";

    /**
     * Initialize this GenericUDF. This will be called once and only once per
     * GenericUDF instance.
     *
     * @param arguments The ObjectInspector for the arguments
     * @return The ObjectInspector for the return value
     * @throws UDFArgumentException Thrown when arguments have wrong types, wrong length, etc.
     */
    @Override
    public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
        assert(arguments.length == 1);
        return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
    }

    /**
     * Evaluate the GenericUDF with the arguments.
     *
     * @param arguments The arguments as DeferedObject, use DeferedObject.get() to get the
     *                  actual argument Object. The Objects can be inspected by the
     *                  ObjectInspectors passed in the initialize call.
     * @return The
     */
    @Override
    public Object evaluate(DeferredObject[] arguments) throws HiveException {
        String data = String.valueOf(arguments[0].get());
//        logger.info(data);
//        logger.info(encode(data));
//        logger.info(new Text(encode(data)));
//        logger.info("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
//        return new Text("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
        return new Text(encode(data));
    }

    String encode(String data) {
        try {
//            byte[] cipherText = cipher.doFinal(data.getBytes());
            byte[] cipherText = RSAUtils.encryptByPublicKey(data.getBytes(), PUBLIC_KEY);
            return Base64.encodeBase64String(cipherText).replaceAll("\r\n", "");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the String to be displayed in explain.
     *
     * @param children
     */
    @Override
    public String getDisplayString(String[] children) {
        return "GenerateIdUDF";
    }
}
