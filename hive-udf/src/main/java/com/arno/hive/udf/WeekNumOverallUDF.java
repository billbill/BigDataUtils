package com.arno.hive.udf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hive.ql.exec.UDF;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by arno on 16/2/3.
 */
public class WeekNumOverallUDF extends UDF {

    public String evaluate(String date, String format) {

        try {
            SimpleDateFormat sf = new SimpleDateFormat(format);
            Date d = sf.parse(date);
            d.setHours(0);
            d.setMinutes(0);
            d.setSeconds(0);

            Date base = new Date(2010-1900, 01 - 1, 04); // 第一周

            Long delta = d.getTime() - base.getTime();
            delta = delta / (1000 * 86400);
            delta = delta / 7;
            delta += 1;
            return delta.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
